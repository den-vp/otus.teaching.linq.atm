﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата
        public User GetUserInfo(string login, string password)
        {
            var user = Users.SingleOrDefault(u => u.Login.Equals(login) && u.Password.Equals(password));
            if (user == null)
            {
                throw new InvalidOperationException("Пользователь не найден!");
            }
            return user;
        }

        public List<Account> GetAccOfUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }
            var _accounts = from u in Accounts
                            where u.UserId.Equals(user.Id)
                            select u;
            return _accounts.ToList();
        }

        public Dictionary<Account, List<OperationsHistory>> GetAccHistoryOfUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }
            var accounts = GetAccOfUser(user);
            return accounts.GroupJoin(History, a => a.Id, h => h.AccountId,
                (account, history) => KeyValuePair.Create(account, history))
                .ToDictionary(k => k.Key, v => v.Value.ToList());

        }

        private User GetUserById(int id)
        {
            var user = Users.SingleOrDefault(x => x.Id == id);
            if (user == null)
            {
                throw new InvalidOperationException("Пользователь не найден");
            }
            return user;
        }
        private Account GetAccById(int id)
        {
            var acc = Accounts.SingleOrDefault(x => x.Id == id);
            if (acc == null)
            {
                throw new InvalidOperationException("Счет не найден");
            }
            return acc;
        }
        public User GetUserByAccId(int acc)
        {
            return GetUserById(GetAccById(acc).UserId);
        }
        public void GetAllInOperByUser()
        {
            var _history = from history in History
                           where history.OperationType.Equals(OperationType.InputCash)
                           join acc in Accounts on history.AccountId equals acc.Id
                           join user in Users on acc.UserId equals user.Id
                           select new
                           {
                               owner = new
                               {
                                   accountId = acc.Id,
                                   firstName = user.FirstName,
                                   middleName = user.MiddleName,
                                   surName = user.SurName
                               },
                               operDate = history.OperationDate,
                               operType = history.OperationType,
                               operSum = history.CashSum
                           };
            var _historyGroup = _history
                .GroupBy(x => x.owner)
                .Select(x => new
                {
                    owner = x.Key,
                    HistoryAcc = x.Select(x => x)
                });

            foreach (var item in _historyGroup)
            {
                Console.WriteLine($"Пользователь: {item.owner.firstName} {item.owner.middleName} {item.owner.surName}; Счет: {item.owner.accountId}");
                foreach (var h in item.HistoryAcc)
                {
                    Console.WriteLine($"        Тип операции: {h.operType}; Дата операции: {h.operDate}; сумма: {h.operSum}");
                }
                Console.WriteLine();
            }

        }

        public void GetAllUserByCashN(decimal n)
        {
            var account = from acc in Accounts
                          where acc.CashAll > n
                          join u in Users on acc.UserId equals u.Id
                          select new
                          {
                              owner = new
                              {
                                  firstName = u.FirstName,
                                  middleName = u.MiddleName,
                                  surName = u.SurName
                              },
                              accountId = acc.Id,
                              openingDate = acc.OpeningDate,
                              cashAll = acc.CashAll
                          };
            var accountGroup = account
                .GroupBy(x => x.owner)
                .Select(x => new
                {
                    owner = x.Key,
                    accounts = x.Select(x => x)
                });

            foreach (var item in accountGroup)
            {
                Console.WriteLine($"Пользователь: {item.owner.firstName} {item.owner.middleName} {item.owner.surName}");
                foreach (var acc in item.accounts)
                {
                    Console.WriteLine($"        Счет: {acc.accountId}; Дата: {acc.openingDate}; сумма: {acc.cashAll}");
                }
                Console.WriteLine();
            }

        }
    }
}