﻿using System.Linq;
using Otus.Teaching.Linq.Atm;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    using System;
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Старт приложения-банкомата...");

            Console.Write("Введите Логин: ");
            string login = Console.ReadLine();
            Console.Write("Введите Пароль: ");
            string password = Console.ReadLine();

            try
            {
                var atmManager = CreateATMManager();
                var printATMService = new PrintATMService(atmManager);

                //TODO: Далее выводим результаты разработанных LINQ запросов
                Console.WriteLine($"1 Вывод информации о заданном аккаунте по логину и паролю: {login};");
                var user = printATMService.GetUser(login, password);
                
                Console.WriteLine($"2 Вывод данных о всех счетах пользователя {login};");
                printATMService.GetAllAcc(user);

                Console.WriteLine($"3 Вывод данных о всех счетах пользователя {login}, включая историю по каждому счёту;");
                printATMService.GetAccHistory(user);

                Console.WriteLine($"4 Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;");
                printATMService.GetAllInOperByUser();

                Console.WriteLine($"5 Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);");
                printATMService.GetAllUserByCash();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Произошла ошибка: {ex.Message}");
            }
            Console.WriteLine("Завершение работы приложения-банкомата...");
            Console.ReadKey();
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}