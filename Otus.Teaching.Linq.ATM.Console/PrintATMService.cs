﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.Atm
{
    class PrintATMService
    {
        ATMManager _manager;

        public PrintATMService(ATMManager manager)
        {
            _manager = manager;
        }

        public User GetUser(string login, string password)
        {
            var user = _manager.GetUserInfo(login, password);
            Console.WriteLine(user);
            Console.WriteLine();
            return user;
        }

        public void GetAllAcc(User user)
        {
            var accounts = _manager.GetAccOfUser(user);
            foreach (var acc in accounts)
            {
                Console.WriteLine(acc);
            }
            Console.WriteLine();
        }

        public void GetAccHistory(User user)
        {
            var accHistory = _manager.GetAccHistoryOfUser(user);
            foreach (var _accHistory in accHistory)
            {
                Console.WriteLine(_accHistory.Key);
                if (_accHistory.Value.Count ==0)
                {
                    Console.WriteLine("По счету нет операций!");
                    continue;
                }

                foreach (var operHistory in _accHistory.Value)
                {
                    Console.WriteLine(operHistory);
                }
                Console.WriteLine();
            }
        }

        public void GetAllInOperByUser()
        {
            _manager.GetAllInOperByUser();
            Console.WriteLine();
        }

        public void GetAllUserByCash()
        {
            Console.Write("N = ");
            decimal cashN;
            while (!Decimal.TryParse(Console.ReadLine(), out cashN))
            {
                Console.WriteLine("Нужно вводить число!");
            }
            _manager.GetAllUserByCashN(cashN);
            Console.WriteLine();
        }
    }
}